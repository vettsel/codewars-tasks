# В этой задаче мне на вход дается количество секунд, например, 3662 -> вывожу строку вида "1 hour, 1 minute and 2 seconds"
# 1 -> "1 second"
# 61 -> "1 minute and 1 second"
# Учитываем множественное число и порядок "," и "and"
def format_duration(seconds)
  time = ["year", "day", "hour", "minute", "second"]
  temp_str = []
  temp_str.unshift(seconds % 60)
  seconds = seconds / 60
  temp_str.unshift(seconds % 60)
  seconds = seconds / 60
  temp_str.unshift(seconds % 24)
  seconds = seconds / 24
  temp_str.unshift(seconds % 365)
  seconds = seconds / 365
  temp_str.unshift(seconds)
  temp_str.map
  temp_str = temp_str.map.with_index do |el, i|
    if el == 1
      el.to_s + " " + time[i]
    elsif el > 1
      el.to_s + " " + time[i] + "s"
    end
  end
  temp_str = temp_str.compact
  if temp_str.size == 0
    return "now"
  elsif temp_str.size == 1
    return temp_str.join
  else    
    temp_str[-2] = temp_str[-2..-1].join(' and ')
    temp_str.pop
    if temp_str.size == 1
      return temp_str.join
    else
      temp_str.join(", ")
    end
  end
end
