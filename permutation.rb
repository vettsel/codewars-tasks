# Generate all permutations of n elements
# Example: 3 -> [[0, 1, 2], [0, 2, 1], [1, 0, 2], [1, 2, 0], [2, 0, 1], [2, 1, 0]]
def permutation(n)
  if n == 1
    return a = [[0]]
  else  
    a = permutation(n-1)
  end
  inner_size =  a[0].size
  b = []
  (0..inner_size).each do
    a.each { |elem| b << elem.dup }
  end
  insert_number = -1
  (0...b.size).each do |i|
    if i % a.size == 0
      insert_number += 1
    end
    (0...inner_size).each do |j|
      if b[i][j] >= insert_number
        b[i][j] += 1
      end
    end  
    b[i].unshift(insert_number)
  end
  a = b
  return a
end

# !!!
def pm(n)
  return [] if n == 0
  return [[0]] if n == 1

  [].tap do |out|
    pm(n-1).each do |sub|
      n.times do |i|
        out << sub.dup.insert(i-=1, n-1)
      end
    end
  end
end